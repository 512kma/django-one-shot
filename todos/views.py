from django.shortcuts import render, get_object_or_404, redirect
from .models import TodoList, TodoItem
from .forms import TodoForm, TodoFormItem

# Create your views here.
def todo_list_list(request):
    todo_list = TodoList.objects.all()
    context = {"todo_list_list": todo_list}
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todo_items = get_object_or_404(TodoList, id=id)

    context = {"todo_items": todo_items}
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoForm()
    context = {"form": form}

    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    todo_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=todo_list)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)

    else:
        form = TodoForm(instance=todo_list)

    context = {"form": form, "todo_list": todo_list}

    return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    todo_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo_list.delete()
        return redirect("todo_list")

    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        form = TodoFormItem(request.POST)
        if form.is_valid():
            i = form.save()
            return redirect("todo_list_detail", id=i.list.id)
    else:
        form = TodoFormItem()

    context = {"form": form}
    return render(request, "todos/create_item.html", context)


def todo_item_update(request, id):
    todo_item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoFormItem(request.POST, instance=todo_item)
        if form.is_valid():
            i = form.save()
            return redirect("todo_list_detail", id=i.list.id)
    else:
        form = TodoFormItem()
    context = {"form": form}
    return render(request, "todo/update_item.html", context)
